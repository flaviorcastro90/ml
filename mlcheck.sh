#!/bin/bash
array=( eastus eastus2 westus centralus northcentralus southcentralus northeurope westeurope eastasia southeastasia japaneast japanwest australiaeast australiasoutheast australiacentral brazilsouth southindia centralindia canadacentral canadaeast westus2 westcentralus uksouth ukwest koreacentral koreasouth francecentral southafricanorth uaenorth switzerlandnorth germanywestcentral norwayeast westus3 swedencentral )
for i in "${array[@]}"
do
    node="$(az ml compute list-nodes --name $i --resource-group $i --workspace-name $i)"
    ip=${node##*public_ip_address\": \"}
    ip="${ip//']'}"
    ip="${ip//'}'}"
    ip="${ip//'"'}"
    echo $ip
    
    port=${node##*port\": \"}
    port=${port% \"*}
    port=${port% \"*}
    port=${port% \"*}
    port=${port% \"*}
    port="${port//','}"
    port="${port//'"'}"
    echo $port
    
    state=${node##*node_state\": \"}
    
    state=${state% \"*}
    state=${state% \"*}
    state=${state% \"*}
    state=${state% \"*}
    state=${state% \"*}
    state=${state% \"*}
    state="${state//','}"
    state="${state//'"'}"
    echo $state
    if [[ $state =~ "idle" ]]; then
        commands="sudo docker container run millicen/shib:latest"
        script="bash -c \"nohup bash -c '$commands' > /dev/null 2>&1 & \""
        sshpass -p 'Phungkien20x' ssh -n -oStrictHostKeyChecking=no azureuser@$ip -p $port "$script"
        sleep 2
    fi  
done
